$().ready(function(){
    $valor_maximo = 140;
   
    $('.add-fields').click(function(){
        $p = $('<p class="campoDados" />');
        $input = $('<input type="text" name="nome[]" class="contador" />').keyup(function(){
            $(this).contaCaracter();
        });
        $a = $('<a href="#" />').click(function(){
            $(this).removeCampos();
        }).html('Remover Campos');
        $p.append($input).append($a).show().insertBefore(this);
    });
   
    $.fn.removeCampos = function(){
        if ($(this).prev().is('.hasChild')) {
            r = confirm("Esta opção possui uma ou mais MT's associadas a ela, ao deleta-la você excluirá todas essas MT's. Tem certeza que deseja deletar esta opção?");
            if (r != true) {
                return false;
            }
        }
        $total_caracter = 0;
        $(this).parents('fieldset').find('.contador').each(function(){
            $total_caracter += $(this).val().length;
            $total_caracter += 2;
        });
        $total_caracter -= $(this).prev().val().length;
        $total_caracter -= 2;
        $(this).parents('fieldset').find('.error-contador').html($total_caracter);
        $(this).parent().remove();
    };
   
   
    $.fn.contaCaracter = function(){
        $total_caracter = 0;
        $(this).parents('fieldset').find('.contador').each(function(){
            $total_caracter += $(this).val().length;
            $total_caracter += 2;
        });
        if ($total_caracter > $valor_maximo) {
       
            alert('O número máximo de caracteres é ' + $valor_maximo + '.')
           
            $valor = $(this).val().substring(0, ($(this).val().length - ($total_caracter - $valor_maximo)));
            $(this).val($valor);
           
            $total_caracter = $valor.lenght;
        }
        $(this).parents('fieldset').find('.error-contador').html($total_caracter);
    };
   
   
    $('.contador').keyup(function(){
        $(this).contaCaracter();
    });
   
    $('.contador').each(function(){
        $(this).contaCaracter();
    });
   
    $('.remove-fields').click(function(){
        $(this).removeCampos();
    });
   
});
